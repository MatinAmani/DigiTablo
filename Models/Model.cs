﻿using System.Collections.Generic;

namespace DigiTablo.Models
{
    public interface IModel<T>
    {
        bool Delete();
        bool Save();
    }
}