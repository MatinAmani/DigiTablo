﻿using System.Collections.Generic;
using MySqlConnector;

namespace DigiTablo.Models
{
    public class User : IModel<User>
    {
        private static readonly string Table = "users";

        public int id;
        public string name;
        public string username;
        public string password;
        public bool admin;

        public static List<User> All()
        {
            List<User> users = new List<User>();
            MySqlDataReader reader = Program.Database.Get(Table);

            while (reader.Read())
            {
                User user = CreateFromReader(reader);
                users.Add(user);
            }

            reader.Close();
            return users;
        }

        public static User Find(int id)
        {
            MySqlDataReader reader = Program.Database.Get(Table, id);

            if (!reader.HasRows)
            {
                return null;
            }

            reader.Read();
            User user = CreateFromReader(reader);
            reader.Close();
            return user;
        }

        public static List<User> Where(string field, string value, bool contains = false)
        {
            List<User> users = new List<User>();
            MySqlDataReader reader = Program.Database.Get(Table, field, value, contains);

            while (reader.Read())
            {
                User user = CreateFromReader(reader);
                users.Add(user);
            }

            reader.Close();
            return users;
        }

        public bool Delete()
        {
            return Program.Database.Delete(Table, this.id);
        }

        public bool Save()
        {
            string values = "(";
            values = values + "'" + this.name + "', ";
            values = values + "'" + this.username + "', ";
            values = values + "'" + this.password + "', ";
            values = values + (this.admin ? "1" : "0");
            values = values + ");";

            return Program.Database.Insert(Table, new string[]
            {
                "Name", "Username", "Password", "Admin",
            }, values);
        }

        private static User CreateFromReader(MySqlDataReader reader)
        {
            return new User
            {
                id = reader.GetInt32(0),
                name = reader.GetString(1),
                username = reader.GetString(2),
                password = reader.GetString(3),
                admin = reader.GetBoolean(4)
            };
        }
    }
}