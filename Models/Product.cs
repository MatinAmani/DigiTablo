﻿using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace DigiTablo.Models
{
    public class Product : IModel<Product>
    {
        private static readonly string Table = "products";

        public int id;
        public string picture;
        public string title;
        public string description = null;
        public int price;
        public bool discounted = false;
        public int discount = 0;
        public string category;

        public static List<Product> All()
        {
            List<Product> products = new List<Product>();
            MySqlDataReader reader = Program.Database.Get(Table);

            while (reader.Read())
            {
                Product product = CreateFromReader(reader);
                products.Add(product);
            }

            reader.Close();
            return products;
        }

        public static Product Find(int id)
        {
            MySqlDataReader reader = Program.Database.Get(Table, id);
            reader.Read();
            Product product = CreateFromReader(reader);
            reader.Close();
            return product;
        }

        public static List<Product> Where(string field, string value, bool contains = false)
        {
            List<Product> products = new List<Product>();
            MySqlDataReader reader = Program.Database.Get(Table, field, value, contains);

            while (reader.Read())
            {
                Product product = CreateFromReader(reader);
                products.Add(product);
            }

            reader.Close();
            return products;
        }

        public void SetPicture(string path = null)
        {
            if (path != null)
            {
                byte[] imageBytes = System.IO.File.ReadAllBytes(path);
                this.picture = Convert.ToBase64String(imageBytes);
            }
        }

        public Bitmap GetPicture()
        {
            if (string.IsNullOrEmpty(this.picture))
            {
                return null;
            }
            byte[] bytes = Convert.FromBase64String(this.picture);
            MemoryStream memory = new MemoryStream(bytes) {Position = 0};
            Bitmap bitmap = (Bitmap) Image.FromStream(memory);
            memory.Close();
            return bitmap;
        }

        public bool Save()
        {
            List<string> columns = new List<string>
            {
                "Title",
                "Price",
                "Category",
                "Discounted",
                "Discount",
                "Description",
                "Picture"
            };

            string values = "(";
            values = values + "'" + this.title + "', ";
            values = values + this.price + ", ";
            values = values + "'" + this.category + "', ";
            values = values + (this.discounted ? "1" : "0") + ", ";
            values = values + this.discount + ", ";
            values = values + "'" + this.description + "', ";
            values = values + "'" + this.picture + "'";
            values = values + ");";

            return Program.Database.Insert(Table, columns.ToArray(), values);
        }

        public bool Delete()
        {
            return Program.Database.Delete(Table, this.id);
        }

        private static Product CreateFromReader(MySqlDataReader reader)
        {
            return new Product
            {
                id = reader.GetInt32(0),
                picture = reader.GetString(1),
                title = reader.GetString(2),
                description = reader.GetString(3),
                price = reader.GetInt32(4),
                discounted = reader.GetBoolean(5),
                discount = reader.GetInt32(6),
                category = reader.GetString(7)
            };
        }
    }
}