﻿namespace DigiTablo.Views
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.Main_menuStrip = new System.Windows.Forms.MenuStrip();
            this.Cart_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ProductAdding_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Login_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AboutUs_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit_toolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DigiTablo_label = new System.Windows.Forms.Label();
            this.Search_textBox = new System.Windows.Forms.TextBox();
            this.btn_Search = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Book = new System.Windows.Forms.Button();
            this.btn_Food = new System.Windows.Forms.Button();
            this.btn_Makeup = new System.Windows.Forms.Button();
            this.btn_Furniture = new System.Windows.Forms.Button();
            this.btn_Works = new System.Windows.Forms.Button();
            this.btn_Shoes = new System.Windows.Forms.Button();
            this.btn_Laptop = new System.Windows.Forms.Button();
            this.btn_CellPhone = new System.Windows.Forms.Button();
            this.DiscountedItem_panel = new System.Windows.Forms.Panel();
            this.ShowAll_button = new System.Windows.Forms.Button();
            this.Server_lable = new System.Windows.Forms.Label();
            this.ConnectionStatus_label = new System.Windows.Forms.Label();
            this.Refresh_button = new System.Windows.Forms.Button();
            this.Main_menuStrip.SuspendLayout();
            this.panel1.SuspendLayout();
            this.DiscountedItem_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // Main_menuStrip
            // 
            this.Main_menuStrip.BackColor = System.Drawing.Color.SeaShell;
            this.Main_menuStrip.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.Main_menuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.Main_menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Cart_toolStripMenuItem,
            this.ProductAdding_toolStripMenuItem,
            this.Login_toolStripMenuItem,
            this.AboutUs_toolStripMenuItem,
            this.Exit_toolStripMenuItem});
            this.Main_menuStrip.Location = new System.Drawing.Point(0, 0);
            this.Main_menuStrip.Name = "Main_menuStrip";
            this.Main_menuStrip.Padding = new System.Windows.Forms.Padding(5, 1, 0, 1);
            this.Main_menuStrip.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Main_menuStrip.Size = new System.Drawing.Size(972, 27);
            this.Main_menuStrip.TabIndex = 3;
            this.Main_menuStrip.Text = "menuStrip1";
            // 
            // Cart_toolStripMenuItem
            // 
            this.Cart_toolStripMenuItem.Name = "Cart_toolStripMenuItem";
            this.Cart_toolStripMenuItem.Size = new System.Drawing.Size(80, 25);
            this.Cart_toolStripMenuItem.Text = "سبد خرید";
            this.Cart_toolStripMenuItem.Click += new System.EventHandler(this.Cart_toolStripMenuItem_Click);
            // 
            // ProductAdding_toolStripMenuItem
            // 
            this.ProductAdding_toolStripMenuItem.Name = "ProductAdding_toolStripMenuItem";
            this.ProductAdding_toolStripMenuItem.Size = new System.Drawing.Size(153, 25);
            this.ProductAdding_toolStripMenuItem.Text = "اضافه کردن محصول";
            this.ProductAdding_toolStripMenuItem.Click += new System.EventHandler(this.ProductAdding_toolStripMenuItem_Click);
            // 
            // Login_toolStripMenuItem
            // 
            this.Login_toolStripMenuItem.Name = "Login_toolStripMenuItem";
            this.Login_toolStripMenuItem.Size = new System.Drawing.Size(166, 25);
            this.Login_toolStripMenuItem.Text = "ورود به حساب کاربری";
            this.Login_toolStripMenuItem.Click += new System.EventHandler(this.Login_toolStripMenuItem_Click);
            // 
            // AboutUs_toolStripMenuItem
            // 
            this.AboutUs_toolStripMenuItem.Name = "AboutUs_toolStripMenuItem";
            this.AboutUs_toolStripMenuItem.Size = new System.Drawing.Size(92, 25);
            this.AboutUs_toolStripMenuItem.Text = "ارتباط با ما";
            this.AboutUs_toolStripMenuItem.Click += new System.EventHandler(this.AboutUs_toolStripMenuItem_Click);
            // 
            // Exit_toolStripMenuItem
            // 
            this.Exit_toolStripMenuItem.Name = "Exit_toolStripMenuItem";
            this.Exit_toolStripMenuItem.Size = new System.Drawing.Size(62, 25);
            this.Exit_toolStripMenuItem.Text = "خروج";
            this.Exit_toolStripMenuItem.Click += new System.EventHandler(this.Exit_toolStripMenuItem1_Click);
            // 
            // DigiTablo_label
            // 
            this.DigiTablo_label.AutoSize = true;
            this.DigiTablo_label.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DigiTablo_label.Location = new System.Drawing.Point(11, 47);
            this.DigiTablo_label.Name = "DigiTablo_label";
            this.DigiTablo_label.Size = new System.Drawing.Size(121, 31);
            this.DigiTablo_label.TabIndex = 4;
            this.DigiTablo_label.Text = "DigiTablo";
            // 
            // Search_textBox
            // 
            this.Search_textBox.Location = new System.Drawing.Point(149, 52);
            this.Search_textBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Search_textBox.Name = "Search_textBox";
            this.Search_textBox.Size = new System.Drawing.Size(325, 22);
            this.Search_textBox.TabIndex = 5;
            // 
            // btn_Search
            // 
            this.btn_Search.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Search.BackgroundImage")));
            this.btn_Search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Search.Location = new System.Drawing.Point(481, 44);
            this.btn_Search.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(51, 37);
            this.btn_Search.TabIndex = 6;
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btn_Book);
            this.panel1.Controls.Add(this.btn_Food);
            this.panel1.Controls.Add(this.btn_Makeup);
            this.panel1.Controls.Add(this.btn_Furniture);
            this.panel1.Controls.Add(this.btn_Works);
            this.panel1.Controls.Add(this.btn_Shoes);
            this.panel1.Controls.Add(this.btn_Laptop);
            this.panel1.Controls.Add(this.btn_CellPhone);
            this.panel1.Controls.Add(this.DiscountedItem_panel);
            this.panel1.Location = new System.Drawing.Point(9, 98);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(959, 737);
            this.panel1.TabIndex = 7;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(11, 458);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(199, 27);
            this.label26.TabIndex = 17;
            this.label26.Text = "پیشنهادهای شگفت انگیز";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(821, 359);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 19);
            this.label9.TabIndex = 16;
            this.label9.Text = "کتاب";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(535, 359);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(116, 19);
            this.label8.TabIndex = 15;
            this.label8.Text = "خوردنی و آشامیدنی";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(337, 359);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 19);
            this.label7.TabIndex = 14;
            this.label7.Text = "زیبایی";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(65, 359);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 19);
            this.label6.TabIndex = 13;
            this.label6.Text = "خانه و آشپزخانه";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(793, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 19);
            this.label5.TabIndex = 12;
            this.label5.Text = "کار و استخدام";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(561, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 19);
            this.label4.TabIndex = 11;
            this.label4.Text = "کیف و کفش";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(316, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 19);
            this.label2.TabIndex = 10;
            this.label2.Text = "لپ تاپ و تبلت";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(69, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 19);
            this.label1.TabIndex = 9;
            this.label1.Text = "گوشی موبایل";
            // 
            // btn_Book
            // 
            this.btn_Book.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Book.BackgroundImage")));
            this.btn_Book.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Book.Location = new System.Drawing.Point(741, 213);
            this.btn_Book.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Book.Name = "btn_Book";
            this.btn_Book.Size = new System.Drawing.Size(196, 144);
            this.btn_Book.TabIndex = 8;
            this.btn_Book.UseVisualStyleBackColor = true;
            this.btn_Book.Click += new System.EventHandler(this.btn_Book_Click);
            // 
            // btn_Food
            // 
            this.btn_Food.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Food.BackgroundImage")));
            this.btn_Food.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Food.Location = new System.Drawing.Point(501, 213);
            this.btn_Food.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Food.Name = "btn_Food";
            this.btn_Food.Size = new System.Drawing.Size(196, 144);
            this.btn_Food.TabIndex = 7;
            this.btn_Food.UseVisualStyleBackColor = true;
            this.btn_Food.Click += new System.EventHandler(this.btn_Food_Click);
            // 
            // btn_Makeup
            // 
            this.btn_Makeup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Makeup.BackgroundImage")));
            this.btn_Makeup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Makeup.Location = new System.Drawing.Point(261, 213);
            this.btn_Makeup.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Makeup.Name = "btn_Makeup";
            this.btn_Makeup.Size = new System.Drawing.Size(196, 144);
            this.btn_Makeup.TabIndex = 6;
            this.btn_Makeup.UseVisualStyleBackColor = true;
            this.btn_Makeup.Click += new System.EventHandler(this.btn_Makeup_Click);
            // 
            // btn_Furniture
            // 
            this.btn_Furniture.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Furniture.BackgroundImage")));
            this.btn_Furniture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Furniture.Location = new System.Drawing.Point(21, 213);
            this.btn_Furniture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Furniture.Name = "btn_Furniture";
            this.btn_Furniture.Size = new System.Drawing.Size(196, 144);
            this.btn_Furniture.TabIndex = 5;
            this.btn_Furniture.UseVisualStyleBackColor = true;
            this.btn_Furniture.Click += new System.EventHandler(this.btn_Furniture_Click);
            // 
            // btn_Works
            // 
            this.btn_Works.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Works.BackgroundImage")));
            this.btn_Works.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Works.Location = new System.Drawing.Point(741, 22);
            this.btn_Works.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Works.Name = "btn_Works";
            this.btn_Works.Size = new System.Drawing.Size(196, 144);
            this.btn_Works.TabIndex = 4;
            this.btn_Works.UseVisualStyleBackColor = true;
            this.btn_Works.Click += new System.EventHandler(this.btn_Works_Click);
            // 
            // btn_Shoes
            // 
            this.btn_Shoes.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Shoes.BackgroundImage")));
            this.btn_Shoes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Shoes.Location = new System.Drawing.Point(501, 22);
            this.btn_Shoes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Shoes.Name = "btn_Shoes";
            this.btn_Shoes.Size = new System.Drawing.Size(196, 144);
            this.btn_Shoes.TabIndex = 3;
            this.btn_Shoes.UseVisualStyleBackColor = true;
            this.btn_Shoes.Click += new System.EventHandler(this.btn_Shoes_Click);
            // 
            // btn_Laptop
            // 
            this.btn_Laptop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Laptop.BackgroundImage")));
            this.btn_Laptop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Laptop.Location = new System.Drawing.Point(261, 22);
            this.btn_Laptop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_Laptop.Name = "btn_Laptop";
            this.btn_Laptop.Size = new System.Drawing.Size(196, 144);
            this.btn_Laptop.TabIndex = 2;
            this.btn_Laptop.UseVisualStyleBackColor = true;
            this.btn_Laptop.Click += new System.EventHandler(this.btn_Laptop_Click);
            // 
            // btn_CellPhone
            // 
            this.btn_CellPhone.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_CellPhone.BackgroundImage")));
            this.btn_CellPhone.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_CellPhone.Location = new System.Drawing.Point(21, 22);
            this.btn_CellPhone.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_CellPhone.Name = "btn_CellPhone";
            this.btn_CellPhone.Size = new System.Drawing.Size(196, 144);
            this.btn_CellPhone.TabIndex = 1;
            this.btn_CellPhone.UseVisualStyleBackColor = true;
            this.btn_CellPhone.Click += new System.EventHandler(this.btn_CellPhone_Click);
            // 
            // DiscountedItem_panel
            // 
            this.DiscountedItem_panel.AutoScroll = true;
            this.DiscountedItem_panel.Controls.Add(this.ShowAll_button);
            this.DiscountedItem_panel.Location = new System.Drawing.Point(7, 498);
            this.DiscountedItem_panel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DiscountedItem_panel.Name = "DiscountedItem_panel";
            this.DiscountedItem_panel.Size = new System.Drawing.Size(949, 229);
            this.DiscountedItem_panel.TabIndex = 0;
            // 
            // ShowAll_button
            // 
            this.ShowAll_button.BackColor = System.Drawing.Color.SeaShell;
            this.ShowAll_button.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShowAll_button.Location = new System.Drawing.Point(1521, 28);
            this.ShowAll_button.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ShowAll_button.Name = "ShowAll_button";
            this.ShowAll_button.Size = new System.Drawing.Size(111, 140);
            this.ShowAll_button.TabIndex = 25;
            this.ShowAll_button.Text = "مشاهده همه";
            this.ShowAll_button.UseVisualStyleBackColor = false;
            this.ShowAll_button.Click += new System.EventHandler(this.ShowAll_button_Click);
            // 
            // Server_lable
            // 
            this.Server_lable.AutoSize = true;
            this.Server_lable.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Server_lable.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Server_lable.Location = new System.Drawing.Point(763, 52);
            this.Server_lable.Name = "Server_lable";
            this.Server_lable.Size = new System.Drawing.Size(65, 23);
            this.Server_lable.TabIndex = 10;
            this.Server_lable.Text = "Server:";
            // 
            // ConnectionStatus_label
            // 
            this.ConnectionStatus_label.AutoSize = true;
            this.ConnectionStatus_label.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConnectionStatus_label.ForeColor = System.Drawing.Color.Green;
            this.ConnectionStatus_label.Location = new System.Drawing.Point(837, 52);
            this.ConnectionStatus_label.Name = "ConnectionStatus_label";
            this.ConnectionStatus_label.Size = new System.Drawing.Size(0, 23);
            this.ConnectionStatus_label.TabIndex = 11;
            // 
            // Refresh_button
            // 
            this.Refresh_button.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Refresh_button.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Refresh_button.BackgroundImage")));
            this.Refresh_button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Refresh_button.Location = new System.Drawing.Point(707, 44);
            this.Refresh_button.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Refresh_button.Name = "Refresh_button";
            this.Refresh_button.Size = new System.Drawing.Size(51, 37);
            this.Refresh_button.TabIndex = 12;
            this.Refresh_button.UseVisualStyleBackColor = false;
            this.Refresh_button.Click += new System.EventHandler(this.Refresh_button_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.ClientSize = new System.Drawing.Size(972, 839);
            this.Controls.Add(this.Refresh_button);
            this.Controls.Add(this.ConnectionStatus_label);
            this.Controls.Add(this.Server_lable);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_Search);
            this.Controls.Add(this.Search_textBox);
            this.Controls.Add(this.DigiTablo_label);
            this.Controls.Add(this.Main_menuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.Main_menuStrip;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DigiTablo";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Main_menuStrip.ResumeLayout(false);
            this.Main_menuStrip.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.DiscountedItem_panel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip Main_menuStrip;
        private System.Windows.Forms.ToolStripMenuItem Cart_toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Login_toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AboutUs_toolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit_toolStripMenuItem;
        private System.Windows.Forms.Label DigiTablo_label;
        private System.Windows.Forms.TextBox Search_textBox;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel DiscountedItem_panel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Book;
        private System.Windows.Forms.Button btn_Food;
        private System.Windows.Forms.Button btn_Makeup;
        private System.Windows.Forms.Button btn_Furniture;
        private System.Windows.Forms.Button btn_Works;
        private System.Windows.Forms.Button btn_Shoes;
        private System.Windows.Forms.Button btn_Laptop;
        private System.Windows.Forms.Button btn_CellPhone;
        private System.Windows.Forms.Button ShowAll_button;
        private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label Server_lable;
		private System.Windows.Forms.Label ConnectionStatus_label;
		private System.Windows.Forms.Button Refresh_button;
		private System.Windows.Forms.ToolStripMenuItem ProductAdding_toolStripMenuItem;
	}
}

