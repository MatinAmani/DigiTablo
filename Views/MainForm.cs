﻿using DigiTablo.Models;
using DigiTablo.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace DigiTablo.Views
{
	public partial class MainForm : Form
	{
		private readonly List<Product> _discountedProducts = new List<Product>();

		public MainForm()
		{
			InitializeComponent();
		}

		private void LoadProducts()
		{
			_discountedProducts.AddRange(Product.Where("discounted", "1"));
		}

		private void Exit_toolStripMenuItem1_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void AboutUs_toolStripMenuItem_Click(object sender, EventArgs e)
		{
			new AboutUs().ShowDialog();
		}

		private void Login_toolStripMenuItem_Click(object sender, EventArgs e)
		{
			new Login().ShowDialog();
		}

		private void Cart_toolStripMenuItem_Click(object sender, EventArgs e)
		{
			new Cart().ShowDialog();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			CheckConnection();
			LoadProducts();
			DiscountedItemsShow();
		}

		private void Refresh_button_Click(object sender, EventArgs e)
		{
			CheckConnection();
		}

		private void CheckConnection()
		{
			ConnectionStatus_label.Text = Resources.loading_indicator;
			ConnectionStatus_label.ForeColor = Color.Gray;

			if (Program.Database.TestConnection())
			{
				ConnectionStatus_label.Text = Resources.db_connected_indicator;
				ConnectionStatus_label.ForeColor = Color.Green;
			}
			else
			{
				ConnectionStatus_label.Text = Resources.db_disconnected_indicator;
				ConnectionStatus_label.ForeColor = Color.Red;
			}
		}

		private void ProductAdding_toolStripMenuItem_Click(object sender, EventArgs e)
		{
			Program.OpenProductAdding();
		}

		private void DiscountedItemsShow()
		{
			for (int i = 0; i < _discountedProducts.Count; i++)
			{
				if (i == 8)
				{
					break;
				}

				int x = 3 + i * 161;
				DiscountedItem_AddToList(x, 7, _discountedProducts[i]);
			}
		}

		private void DiscountedItem_AddToList(int x, int y, Product p)
		{
			float discountedPrice = p.price - p.price * ((float)p.discount / 100);

			Panel panel = CreatePanel(x, y);
			PictureBox pictureBox = CreatePictureBox(p);
			Label title = CreateLabel(p.title, 3, 98);
			Label price = CreateLabel("$" + discountedPrice, 3, 122);

			panel.Controls.Add(pictureBox);
			panel.Controls.Add(title);
			panel.Controls.Add(price);
		}

		private Panel CreatePanel(int x, int y)
		{
			Panel panel = new Panel
			{
				Size = new Size(155, 155),
				Location = new Point(x, y)
			};
			DiscountedItem_panel.Controls.Add(panel);
			return panel;
		}

		private PictureBox CreatePictureBox(Product product)
		{
			Bitmap image = product.GetPicture();

			PictureBox pictureBox = new PictureBox
			{
				Size = new Size(149, 90),
				Location = new Point(3, 3),
				BackColor = Color.Gray
			};

			if (image != null)
			{
				pictureBox.Image = image;
			}

			pictureBox.Click += (sender, args) => { new SingleProduct(product).ShowDialog(); };

			return pictureBox;
		}

		private Label CreateLabel(string title, int x, int y)
		{
			Label label = new Label
			{
				Text = title,
				Font = new Font("Calibri", 14),
				Location = new Point(x, y)
			};
			return label;
		}

		private void btn_Search_Click(object sender, EventArgs e)
		{
			string query = Search_textBox.Text;
			if (query.Length == 0)
			{
				MessageBox.Show("لطفا در کادر چیزی وارد کنید!", "اخطار", MessageBoxButtons.OK);
				return;
			}

			new Result("title", query, true).ShowDialog();
		}

		private void btn_CellPhone_Click(object sender, EventArgs e)
		{
			new Result("Category", "mobile").ShowDialog();
		}

		private void btn_Laptop_Click(object sender, EventArgs e)
		{
			new Result("Category", "laptop-pc").ShowDialog();
		}

		private void btn_Shoes_Click(object sender, EventArgs e)
		{
			new Result("Category", "apparel").ShowDialog();
		}

		private void btn_Works_Click(object sender, EventArgs e)
		{
			new Result("Category", "work").ShowDialog();
		}

		private void btn_Furniture_Click(object sender, EventArgs e)
		{
			new Result("Category", "home-kitchen").ShowDialog();
		}

		private void btn_Makeup_Click(object sender, EventArgs e)
		{
			new Result("Category", "beauty").ShowDialog();
		}

		private void btn_Food_Click(object sender, EventArgs e)
		{
			new Result("Category", "food").ShowDialog();
		}

		private void btn_Book_Click(object sender, EventArgs e)
		{
			new Result("Category", "book").ShowDialog();
		}

		private void ShowAll_button_Click(object sender, EventArgs e)
		{
			new Result("Discounted", "1").ShowDialog();
		}
	}
}