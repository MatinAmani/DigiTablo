﻿using System;
using System.Windows.Forms;
using DigiTablo.Models;

namespace DigiTablo.Views
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

		private void btn_ShowSignUp_Click(object sender, EventArgs e)
		{
            SignUp_panel.Enabled = true;
		}

		private void btn_SignUp_Click(object sender, EventArgs e)
		{
			if(Password_textBox.Text==PassConfirm_textBox.Text)
			{
				User user = new User();
				user.name = Name_textBox.Text + " " + LastName_textBox.Text;
				user.username = Username_textBox.Text;
				user.password = Password_textBox.Text;
				user.admin = Admin_checkBox.Checked;

				if (user.Save())
				{
					MessageBox.Show(
					"User Added",
					"Adding Scceeded",
					MessageBoxButtons.OK,
					MessageBoxIcon.Information
					);
				}
				else
				{
					MessageBox.Show(
					"An Error Occurred in Adding User",
					"Error",
					MessageBoxButtons.OK,
					MessageBoxIcon.Error
					);
				}

				Name_textBox.Text = "";
				LastName_textBox.Text = "";
				Username_textBox.Text = "";
				Password_textBox.Text = "";
				PassConfirm_textBox.Text = "";
				Admin_checkBox.Checked = false;
			}
			else
			{
				MessageBox.Show(
					"Please Confirm Your Password",
					"Error",
					MessageBoxButtons.OK,
					MessageBoxIcon.Error
					);

				Name_textBox.Text = "";
				LastName_textBox.Text = "";
				Username_textBox.Text = "";
				Password_textBox.Text = "";
				PassConfirm_textBox.Text = "";
				Admin_checkBox.Checked = false;
			}
		}
	}
}
