﻿using DigiTablo.Models;
using System;
using System.Drawing;
using System.Windows.Forms;


namespace DigiTablo.Views
{
    public partial class SingleProduct : Form
    {
        private readonly Product _product;

        public SingleProduct(Product product)
        {
            InitializeComponent();
            _product = product;
        }

        private void SingleProduct_Load(object sender, EventArgs e)
        {
            Title_label.Text = _product.title;
            Price_label.Text = _product.price.ToString();
            Category_label.Text = _product.category;
            Description_richTextBox.Text = _product.description;
            if (_product.GetPicture() != null)
            {
                ProductPic_pictureBox.Image = _product.GetPicture();
            }

            if (_product.discounted)
            {
                Price_label.ForeColor = Color.Red;
                DiscountedPrice_label.Text =
                    (_product.price - _product.price * ((float) _product.discount / 100)).ToString();
                Price_label.Font = new Font("Calibri", 16, FontStyle.Strikeout);
                Discounted_pictureBox.Visible = true;
            }
        }

        private void Buy_button_Click(object sender, EventArgs e)
        {
            Program.Cart.Add(_product);
            MessageBox.Show("Added to your cart.", "Added", MessageBoxButtons.OK);
        }
    }
}