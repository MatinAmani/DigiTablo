﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DigiTablo.Models;

namespace DigiTablo.Views
{
    public partial class Cart : Form
    {
        public Cart()
        {
            InitializeComponent();
        }

        private void Cart_Load(object sender, EventArgs e)
        {
            ShowItems();
        }
        
        private void ShowItems()
        {
            for (int i = 0; i < Program.Cart.Count; i++)
            {
                int x = 3 + i * 161;
                AddItem(x, 7, Program.Cart[i]);
            }
        }

        private void AddItem(int x, int y, Product p)
        {
            float discountedPrice = p.price - p.price * ((float) p.discount / 100);

            Panel panel = CreatePanel(x, y);
            PictureBox pictureBox = CreatePictureBox(p);
            Label title = CreateLabel(p.title, 3, 98);
            Label price = CreateLabel("$" + discountedPrice, 3, 122);

            panel.Controls.Add(pictureBox);
            panel.Controls.Add(title);
            panel.Controls.Add(price);
        }

        private Panel CreatePanel(int x, int y)
        {
            Panel panel = new Panel
            {
                Size = new Size(155, 155),
                Location = new Point(x, y)
            };
            panel1.Controls.Add(panel);
            return panel;
        }

        private PictureBox CreatePictureBox(Product product)
        {
            Bitmap image = product.GetPicture();
            
            PictureBox pictureBox = new PictureBox
            {
                Size = new Size(149, 90),
                Location = new Point(3, 3),
                BackColor = Color.Gray
            };

            if (image != null)
            {
                pictureBox.Image = image;
            }

            return pictureBox;
        }

        private Label CreateLabel(string title, int x, int y)
        {
            Label label = new Label
            {
                Text = title,
                Font = new Font("Calibri", 14),
                Location = new Point(x, y)
            };
            return label;
        }
    }
}
