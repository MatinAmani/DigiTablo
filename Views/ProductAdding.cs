﻿using DigiTablo.Models;
using System.Windows.Forms;

namespace DigiTablo.Views
{
    public partial class ProductAdding : Form
    {
        public ProductAdding()
        {
            InitializeComponent();
        }

        private void AddProduct_button_Click(object sender, System.EventArgs e)
        {
            Product product = new Product
            {
                title = Title_textBox.Text,
                category = Category_textBox.Text,
                price = int.Parse(Price_textBox.Text),
                description = Description_richTextBox.Text
            };

            if(Discounted_checkBox.Checked)
			{
                product.discounted = true;
                product.discount = int.Parse(Discount_textBox.Text);
			}

            product.SetPicture(ProductPic_pictureBox.ImageLocation);

            if (product.Save())
            {
                MessageBox.Show(
                    "Product added Successfully.",
                    "Operation Succeeded",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information
                );
                Title_textBox.Text = "";
                Category_textBox.Text = "";
                Price_textBox.Text = "";
                Description_richTextBox.Text = "";
                Discount_textBox.Text = "";
                Discounted_checkBox.Checked = false;
            }
            else
            {
                MessageBox.Show("An Error Occurred", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ProductPic_pictureBox_Click(object sender, System.EventArgs e)
        {
            /*
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = "C:\\";
            openFileDialog.Filter = "PNG files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|All files|*.*";
            openFileDialog.ShowDialog();
            ProductPic_pictureBox.ImageLocation = openFileDialog.FileName;
            */
        }

		private void Discounted_checkBox_CheckedChanged(object sender, System.EventArgs e)
		{
            if(Discounted_checkBox.Checked)
			{
                Discount_textBox.Enabled = true;
			}
			else
			{
                Discount_textBox.Enabled = false;
			}
		}

		private void Clear_button_Click(object sender, System.EventArgs e)
		{
            Title_textBox.Text = "";
            Category_textBox.Text = "";
            Price_textBox.Text = "";
            Description_richTextBox.Text = "";
            Discount_textBox.Text = "";
            Discounted_checkBox.Checked = false;
        }

		private void Exit_button_Click(object sender, System.EventArgs e)
		{
            this.Close();
		}
	}
}