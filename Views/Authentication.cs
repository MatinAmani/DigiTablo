﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DigiTablo.Views
{
	public partial class Authentication : Form
	{
		public Authentication()
		{
			InitializeComponent();
		}

		private void Enter_button_Click(object sender, EventArgs e)
		{
			if(Username_textBox.Text=="admin" && Password_textBox.Text=="admin")
			{
				this.Hide();
				(new ProductAdding()).ShowDialog();
				this.Close();
			}
		}

		private void Exit_button_Click(object sender, EventArgs e)
		{
			this.Close();
		}
	}
}
