﻿namespace DigiTablo.Views
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
			this.btn_Login = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.SignUp_panel = new System.Windows.Forms.Panel();
			this.btn_SignUp = new System.Windows.Forms.Button();
			this.PassConfirm_textBox = new System.Windows.Forms.TextBox();
			this.Password_textBox = new System.Windows.Forms.TextBox();
			this.Username_textBox = new System.Windows.Forms.TextBox();
			this.LastName_textBox = new System.Windows.Forms.TextBox();
			this.Name_textBox = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.btn_ShowSignUp = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.Admin_checkBox = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SignUp_panel.SuspendLayout();
			this.SuspendLayout();
			// 
			// btn_Login
			// 
			this.btn_Login.BackColor = System.Drawing.Color.DodgerBlue;
			this.btn_Login.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_Login.Location = new System.Drawing.Point(198, 95);
			this.btn_Login.Margin = new System.Windows.Forms.Padding(2);
			this.btn_Login.Name = "btn_Login";
			this.btn_Login.Size = new System.Drawing.Size(52, 29);
			this.btn_Login.TabIndex = 0;
			this.btn_Login.Text = "ورود";
			this.btn_Login.UseVisualStyleBackColor = false;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(336, 38);
			this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(58, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "نام کاربری";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(336, 75);
			this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(55, 16);
			this.label2.TabIndex = 2;
			this.label2.Text = "رمز عبور";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(46, 36);
			this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(55, 55);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 3;
			this.pictureBox1.TabStop = false;
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(135, 36);
			this.textBox1.Margin = new System.Windows.Forms.Padding(2);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(179, 20);
			this.textBox1.TabIndex = 4;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(135, 71);
			this.textBox2.Margin = new System.Windows.Forms.Padding(2);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(179, 20);
			this.textBox2.TabIndex = 5;
			// 
			// SignUp_panel
			// 
			this.SignUp_panel.Controls.Add(this.Admin_checkBox);
			this.SignUp_panel.Controls.Add(this.btn_SignUp);
			this.SignUp_panel.Controls.Add(this.PassConfirm_textBox);
			this.SignUp_panel.Controls.Add(this.Password_textBox);
			this.SignUp_panel.Controls.Add(this.Username_textBox);
			this.SignUp_panel.Controls.Add(this.LastName_textBox);
			this.SignUp_panel.Controls.Add(this.Name_textBox);
			this.SignUp_panel.Controls.Add(this.label8);
			this.SignUp_panel.Controls.Add(this.label7);
			this.SignUp_panel.Controls.Add(this.label6);
			this.SignUp_panel.Controls.Add(this.label5);
			this.SignUp_panel.Controls.Add(this.label4);
			this.SignUp_panel.Enabled = false;
			this.SignUp_panel.Location = new System.Drawing.Point(14, 207);
			this.SignUp_panel.Margin = new System.Windows.Forms.Padding(2);
			this.SignUp_panel.Name = "SignUp_panel";
			this.SignUp_panel.Size = new System.Drawing.Size(421, 193);
			this.SignUp_panel.TabIndex = 6;
			// 
			// btn_SignUp
			// 
			this.btn_SignUp.BackColor = System.Drawing.Color.Gold;
			this.btn_SignUp.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_SignUp.Location = new System.Drawing.Point(184, 124);
			this.btn_SignUp.Margin = new System.Windows.Forms.Padding(2);
			this.btn_SignUp.Name = "btn_SignUp";
			this.btn_SignUp.Size = new System.Drawing.Size(53, 30);
			this.btn_SignUp.TabIndex = 12;
			this.btn_SignUp.Text = "ثبت نام";
			this.btn_SignUp.UseVisualStyleBackColor = false;
			this.btn_SignUp.Click += new System.EventHandler(this.btn_SignUp_Click);
			// 
			// PassConfirm_textBox
			// 
			this.PassConfirm_textBox.Location = new System.Drawing.Point(11, 84);
			this.PassConfirm_textBox.Margin = new System.Windows.Forms.Padding(2);
			this.PassConfirm_textBox.Name = "PassConfirm_textBox";
			this.PassConfirm_textBox.Size = new System.Drawing.Size(115, 20);
			this.PassConfirm_textBox.TabIndex = 10;
			// 
			// Password_textBox
			// 
			this.Password_textBox.Location = new System.Drawing.Point(11, 47);
			this.Password_textBox.Margin = new System.Windows.Forms.Padding(2);
			this.Password_textBox.Name = "Password_textBox";
			this.Password_textBox.Size = new System.Drawing.Size(115, 20);
			this.Password_textBox.TabIndex = 9;
			// 
			// Username_textBox
			// 
			this.Username_textBox.Location = new System.Drawing.Point(212, 47);
			this.Username_textBox.Margin = new System.Windows.Forms.Padding(2);
			this.Username_textBox.Name = "Username_textBox";
			this.Username_textBox.Size = new System.Drawing.Size(134, 20);
			this.Username_textBox.TabIndex = 8;
			// 
			// LastName_textBox
			// 
			this.LastName_textBox.Location = new System.Drawing.Point(11, 10);
			this.LastName_textBox.Margin = new System.Windows.Forms.Padding(2);
			this.LastName_textBox.Name = "LastName_textBox";
			this.LastName_textBox.Size = new System.Drawing.Size(115, 20);
			this.LastName_textBox.TabIndex = 7;
			// 
			// Name_textBox
			// 
			this.Name_textBox.Location = new System.Drawing.Point(212, 10);
			this.Name_textBox.Margin = new System.Windows.Forms.Padding(2);
			this.Name_textBox.Name = "Name_textBox";
			this.Name_textBox.Size = new System.Drawing.Size(134, 20);
			this.Name_textBox.TabIndex = 6;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(130, 86);
			this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(86, 16);
			this.label8.TabIndex = 4;
			this.label8.Text = "تکرار رمز عبور";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(130, 49);
			this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(55, 16);
			this.label7.TabIndex = 3;
			this.label7.Text = "رمز عبور";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(350, 49);
			this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(58, 16);
			this.label6.TabIndex = 2;
			this.label6.Text = "نام کاربری";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(130, 12);
			this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 16);
			this.label5.TabIndex = 1;
			this.label5.Text = "نام خانوادگی";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(389, 12);
			this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(19, 16);
			this.label4.TabIndex = 0;
			this.label4.Text = "نام";
			// 
			// btn_ShowSignUp
			// 
			this.btn_ShowSignUp.BackColor = System.Drawing.Color.Gold;
			this.btn_ShowSignUp.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_ShowSignUp.Location = new System.Drawing.Point(250, 164);
			this.btn_ShowSignUp.Margin = new System.Windows.Forms.Padding(2);
			this.btn_ShowSignUp.Name = "btn_ShowSignUp";
			this.btn_ShowSignUp.Size = new System.Drawing.Size(49, 27);
			this.btn_ShowSignUp.TabIndex = 7;
			this.btn_ShowSignUp.Text = "ثبت نام";
			this.btn_ShowSignUp.UseVisualStyleBackColor = false;
			this.btn_ShowSignUp.Click += new System.EventHandler(this.btn_ShowSignUp_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(303, 169);
			this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(125, 17);
			this.label3.TabIndex = 8;
			this.label3.Text = "برای ثبت نام کلیک کنید";
			// 
			// Admin_checkBox
			// 
			this.Admin_checkBox.AutoSize = true;
			this.Admin_checkBox.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Admin_checkBox.Location = new System.Drawing.Point(236, 79);
			this.Admin_checkBox.Name = "Admin_checkBox";
			this.Admin_checkBox.Size = new System.Drawing.Size(79, 27);
			this.Admin_checkBox.TabIndex = 13;
			this.Admin_checkBox.Text = "Admin";
			this.Admin_checkBox.UseVisualStyleBackColor = true;
			// 
			// Login
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Linen;
			this.ClientSize = new System.Drawing.Size(448, 402);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.btn_ShowSignUp);
			this.Controls.Add(this.SignUp_panel);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btn_Login);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Margin = new System.Windows.Forms.Padding(2);
			this.MaximizeBox = false;
			this.Name = "Login";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Login";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.SignUp_panel.ResumeLayout(false);
			this.SignUp_panel.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Login;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Panel SignUp_panel;
        private System.Windows.Forms.Button btn_SignUp;
        private System.Windows.Forms.TextBox PassConfirm_textBox;
        private System.Windows.Forms.TextBox Password_textBox;
        private System.Windows.Forms.TextBox Username_textBox;
        private System.Windows.Forms.TextBox LastName_textBox;
        private System.Windows.Forms.TextBox Name_textBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_ShowSignUp;
        private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox Admin_checkBox;
	}
}