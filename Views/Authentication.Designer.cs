﻿
namespace DigiTablo.Views
{
	partial class Authentication
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Exit_button = new System.Windows.Forms.Button();
			this.Enter_button = new System.Windows.Forms.Button();
			this.Password_textBox = new System.Windows.Forms.TextBox();
			this.Password_label = new System.Windows.Forms.Label();
			this.Username_label = new System.Windows.Forms.Label();
			this.Username_textBox = new System.Windows.Forms.TextBox();
			this.Warning2_label = new System.Windows.Forms.Label();
			this.Warning1_label = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// Exit_button
			// 
			this.Exit_button.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Exit_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Exit_button.Location = new System.Drawing.Point(195, 161);
			this.Exit_button.Name = "Exit_button";
			this.Exit_button.Size = new System.Drawing.Size(95, 36);
			this.Exit_button.TabIndex = 15;
			this.Exit_button.Text = "Exit";
			this.Exit_button.UseVisualStyleBackColor = true;
			this.Exit_button.Click += new System.EventHandler(this.Exit_button_Click);
			// 
			// Enter_button
			// 
			this.Enter_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Enter_button.Location = new System.Drawing.Point(94, 161);
			this.Enter_button.Name = "Enter_button";
			this.Enter_button.Size = new System.Drawing.Size(95, 36);
			this.Enter_button.TabIndex = 14;
			this.Enter_button.Text = "Enter";
			this.Enter_button.UseVisualStyleBackColor = true;
			this.Enter_button.Click += new System.EventHandler(this.Enter_button_Click);
			// 
			// Password_textBox
			// 
			this.Password_textBox.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Password_textBox.Location = new System.Drawing.Point(140, 112);
			this.Password_textBox.Name = "Password_textBox";
			this.Password_textBox.PasswordChar = '*';
			this.Password_textBox.Size = new System.Drawing.Size(215, 33);
			this.Password_textBox.TabIndex = 13;
			// 
			// Password_label
			// 
			this.Password_label.AutoSize = true;
			this.Password_label.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Password_label.Location = new System.Drawing.Point(35, 115);
			this.Password_label.Name = "Password_label";
			this.Password_label.Size = new System.Drawing.Size(99, 26);
			this.Password_label.TabIndex = 12;
			this.Password_label.Text = "Password:";
			// 
			// Username_label
			// 
			this.Username_label.AutoSize = true;
			this.Username_label.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Username_label.Location = new System.Drawing.Point(30, 76);
			this.Username_label.Name = "Username_label";
			this.Username_label.Size = new System.Drawing.Size(104, 26);
			this.Username_label.TabIndex = 11;
			this.Username_label.Text = "Username:";
			// 
			// Username_textBox
			// 
			this.Username_textBox.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Username_textBox.Location = new System.Drawing.Point(140, 73);
			this.Username_textBox.Name = "Username_textBox";
			this.Username_textBox.Size = new System.Drawing.Size(215, 33);
			this.Username_textBox.TabIndex = 10;
			// 
			// Warning2_label
			// 
			this.Warning2_label.AutoSize = true;
			this.Warning2_label.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Warning2_label.Location = new System.Drawing.Point(23, 39);
			this.Warning2_label.Name = "Warning2_label";
			this.Warning2_label.Size = new System.Drawing.Size(339, 26);
			this.Warning2_label.TabIndex = 9;
			this.Warning2_label.Text = "اگر عضو ویژه هستید  اطلاعات خود را وارد کنید";
			// 
			// Warning1_label
			// 
			this.Warning1_label.AutoSize = true;
			this.Warning1_label.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Warning1_label.Location = new System.Drawing.Point(48, 13);
			this.Warning1_label.Name = "Warning1_label";
			this.Warning1_label.Size = new System.Drawing.Size(288, 26);
			this.Warning1_label.TabIndex = 8;
			this.Warning1_label.Text = "این پنجره مخصوص اعضای ویژه میباشد";
			// 
			// Authentication
			// 
			this.AcceptButton = this.Enter_button;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.CancelButton = this.Exit_button;
			this.ClientSize = new System.Drawing.Size(384, 210);
			this.Controls.Add(this.Exit_button);
			this.Controls.Add(this.Enter_button);
			this.Controls.Add(this.Password_textBox);
			this.Controls.Add(this.Password_label);
			this.Controls.Add(this.Username_label);
			this.Controls.Add(this.Username_textBox);
			this.Controls.Add(this.Warning2_label);
			this.Controls.Add(this.Warning1_label);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Authentication";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Authentication";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button Exit_button;
		private System.Windows.Forms.Button Enter_button;
		private System.Windows.Forms.TextBox Password_textBox;
		private System.Windows.Forms.Label Password_label;
		private System.Windows.Forms.Label Username_label;
		private System.Windows.Forms.TextBox Username_textBox;
		private System.Windows.Forms.Label Warning2_label;
		private System.Windows.Forms.Label Warning1_label;
	}
}