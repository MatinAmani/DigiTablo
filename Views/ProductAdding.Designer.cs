﻿
namespace DigiTablo.Views
{
	partial class ProductAdding
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProductAdding));
			this.ProductAdding_panel = new System.Windows.Forms.Panel();
			this.Description_richTextBox = new System.Windows.Forms.RichTextBox();
			this.Description_label = new System.Windows.Forms.Label();
			this.Clear_button = new System.Windows.Forms.Button();
			this.Category_textBox = new System.Windows.Forms.TextBox();
			this.ProductPic_pictureBox = new System.Windows.Forms.PictureBox();
			this.Price_textBox = new System.Windows.Forms.TextBox();
			this.Title_textBox = new System.Windows.Forms.TextBox();
			this.AddProduct_button = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.Discounted_checkBox = new System.Windows.Forms.CheckBox();
			this.Discount_textBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.Exit_button = new System.Windows.Forms.Button();
			this.ProductAdding_panel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ProductPic_pictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// ProductAdding_panel
			// 
			this.ProductAdding_panel.Controls.Add(this.Exit_button);
			this.ProductAdding_panel.Controls.Add(this.label4);
			this.ProductAdding_panel.Controls.Add(this.Discount_textBox);
			this.ProductAdding_panel.Controls.Add(this.Discounted_checkBox);
			this.ProductAdding_panel.Controls.Add(this.Description_richTextBox);
			this.ProductAdding_panel.Controls.Add(this.Description_label);
			this.ProductAdding_panel.Controls.Add(this.Clear_button);
			this.ProductAdding_panel.Controls.Add(this.Category_textBox);
			this.ProductAdding_panel.Controls.Add(this.ProductPic_pictureBox);
			this.ProductAdding_panel.Controls.Add(this.Price_textBox);
			this.ProductAdding_panel.Controls.Add(this.Title_textBox);
			this.ProductAdding_panel.Controls.Add(this.AddProduct_button);
			this.ProductAdding_panel.Controls.Add(this.label3);
			this.ProductAdding_panel.Controls.Add(this.label2);
			this.ProductAdding_panel.Controls.Add(this.label1);
			this.ProductAdding_panel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ProductAdding_panel.Location = new System.Drawing.Point(0, 0);
			this.ProductAdding_panel.Name = "ProductAdding_panel";
			this.ProductAdding_panel.Size = new System.Drawing.Size(634, 460);
			this.ProductAdding_panel.TabIndex = 0;
			// 
			// Description_richTextBox
			// 
			this.Description_richTextBox.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Description_richTextBox.Location = new System.Drawing.Point(357, 71);
			this.Description_richTextBox.Name = "Description_richTextBox";
			this.Description_richTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.Description_richTextBox.Size = new System.Drawing.Size(265, 342);
			this.Description_richTextBox.TabIndex = 6;
			this.Description_richTextBox.Text = "";
			// 
			// Description_label
			// 
			this.Description_label.AutoSize = true;
			this.Description_label.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Description_label.Location = new System.Drawing.Point(352, 42);
			this.Description_label.Name = "Description_label";
			this.Description_label.Size = new System.Drawing.Size(115, 26);
			this.Description_label.TabIndex = 13;
			this.Description_label.Text = "Description:";
			// 
			// Clear_button
			// 
			this.Clear_button.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Clear_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Clear_button.Location = new System.Drawing.Point(154, 377);
			this.Clear_button.Name = "Clear_button";
			this.Clear_button.Size = new System.Drawing.Size(136, 36);
			this.Clear_button.TabIndex = 8;
			this.Clear_button.Text = "Clear";
			this.Clear_button.UseVisualStyleBackColor = true;
			this.Clear_button.Click += new System.EventHandler(this.Clear_button_Click);
			// 
			// Category_textBox
			// 
			this.Category_textBox.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Category_textBox.Location = new System.Drawing.Point(145, 258);
			this.Category_textBox.Name = "Category_textBox";
			this.Category_textBox.Size = new System.Drawing.Size(177, 33);
			this.Category_textBox.TabIndex = 2;
			// 
			// ProductPic_pictureBox
			// 
			this.ProductPic_pictureBox.BackColor = System.Drawing.Color.Gainsboro;
			this.ProductPic_pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("ProductPic_pictureBox.Image")));
			this.ProductPic_pictureBox.Location = new System.Drawing.Point(17, 13);
			this.ProductPic_pictureBox.Name = "ProductPic_pictureBox";
			this.ProductPic_pictureBox.Size = new System.Drawing.Size(305, 200);
			this.ProductPic_pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.ProductPic_pictureBox.TabIndex = 10;
			this.ProductPic_pictureBox.TabStop = false;
			this.ProductPic_pictureBox.Click += new System.EventHandler(this.ProductPic_pictureBox_Click);
			// 
			// Price_textBox
			// 
			this.Price_textBox.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Price_textBox.Location = new System.Drawing.Point(145, 297);
			this.Price_textBox.Name = "Price_textBox";
			this.Price_textBox.Size = new System.Drawing.Size(92, 33);
			this.Price_textBox.TabIndex = 3;
			// 
			// Title_textBox
			// 
			this.Title_textBox.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Title_textBox.Location = new System.Drawing.Point(145, 219);
			this.Title_textBox.Name = "Title_textBox";
			this.Title_textBox.Size = new System.Drawing.Size(177, 33);
			this.Title_textBox.TabIndex = 1;
			// 
			// AddProduct_button
			// 
			this.AddProduct_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.AddProduct_button.Location = new System.Drawing.Point(12, 377);
			this.AddProduct_button.Name = "AddProduct_button";
			this.AddProduct_button.Size = new System.Drawing.Size(136, 36);
			this.AddProduct_button.TabIndex = 7;
			this.AddProduct_button.Text = "Add Product";
			this.AddProduct_button.UseVisualStyleBackColor = true;
			this.AddProduct_button.Click += new System.EventHandler(this.AddProduct_button_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(12, 300);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(60, 26);
			this.label3.TabIndex = 6;
			this.label3.Text = "Price:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(12, 261);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(94, 26);
			this.label2.TabIndex = 5;
			this.label2.Text = "Category:";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(12, 222);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(127, 26);
			this.label1.TabIndex = 4;
			this.label1.Text = "Product Title:";
			// 
			// Discounted_checkBox
			// 
			this.Discounted_checkBox.AutoSize = true;
			this.Discounted_checkBox.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Discounted_checkBox.Location = new System.Drawing.Point(12, 341);
			this.Discounted_checkBox.Name = "Discounted_checkBox";
			this.Discounted_checkBox.Size = new System.Drawing.Size(127, 30);
			this.Discounted_checkBox.TabIndex = 4;
			this.Discounted_checkBox.Text = "Discounted";
			this.Discounted_checkBox.UseVisualStyleBackColor = true;
			this.Discounted_checkBox.CheckedChanged += new System.EventHandler(this.Discounted_checkBox_CheckedChanged);
			// 
			// Discount_textBox
			// 
			this.Discount_textBox.Enabled = false;
			this.Discount_textBox.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Discount_textBox.Location = new System.Drawing.Point(145, 338);
			this.Discount_textBox.Name = "Discount_textBox";
			this.Discount_textBox.Size = new System.Drawing.Size(71, 33);
			this.Discount_textBox.TabIndex = 5;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(222, 341);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(66, 26);
			this.label4.TabIndex = 17;
			this.label4.Text = "% OFF";
			// 
			// Exit_button
			// 
			this.Exit_button.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Exit_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Exit_button.Location = new System.Drawing.Point(80, 419);
			this.Exit_button.Name = "Exit_button";
			this.Exit_button.Size = new System.Drawing.Size(136, 36);
			this.Exit_button.TabIndex = 18;
			this.Exit_button.Text = "Exit";
			this.Exit_button.UseVisualStyleBackColor = true;
			this.Exit_button.Click += new System.EventHandler(this.Exit_button_Click);
			// 
			// ProductAdding
			// 
			this.AcceptButton = this.AddProduct_button;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.MistyRose;
			this.CancelButton = this.Clear_button;
			this.ClientSize = new System.Drawing.Size(634, 460);
			this.Controls.Add(this.ProductAdding_panel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "ProductAdding";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Product Adding";
			this.ProductAdding_panel.ResumeLayout(false);
			this.ProductAdding_panel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.ProductPic_pictureBox)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel ProductAdding_panel;
		private System.Windows.Forms.PictureBox ProductPic_pictureBox;
		private System.Windows.Forms.TextBox Price_textBox;
		private System.Windows.Forms.TextBox Title_textBox;
		private System.Windows.Forms.Button AddProduct_button;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button Clear_button;
		private System.Windows.Forms.TextBox Category_textBox;
		private System.Windows.Forms.Label Description_label;
		private System.Windows.Forms.RichTextBox Description_richTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox Discount_textBox;
		private System.Windows.Forms.CheckBox Discounted_checkBox;
		private System.Windows.Forms.Button Exit_button;
	}
}