﻿
namespace DigiTablo.Views
{
    partial class SingleProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SingleProduct));
			this.ProductPic_pictureBox = new System.Windows.Forms.PictureBox();
			this.Title_label = new System.Windows.Forms.Label();
			this.Price_label = new System.Windows.Forms.Label();
			this.Description_richTextBox = new System.Windows.Forms.RichTextBox();
			this.DiscountedPrice_label = new System.Windows.Forms.Label();
			this.Buy_button = new System.Windows.Forms.Button();
			this.Discounted_pictureBox = new System.Windows.Forms.PictureBox();
			this.Category_label = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.ProductPic_pictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Discounted_pictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// ProductPic_pictureBox
			// 
			this.ProductPic_pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("ProductPic_pictureBox.Image")));
			this.ProductPic_pictureBox.Location = new System.Drawing.Point(11, 11);
			this.ProductPic_pictureBox.Margin = new System.Windows.Forms.Padding(2);
			this.ProductPic_pictureBox.Name = "ProductPic_pictureBox";
			this.ProductPic_pictureBox.Size = new System.Drawing.Size(155, 155);
			this.ProductPic_pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.ProductPic_pictureBox.TabIndex = 1;
			this.ProductPic_pictureBox.TabStop = false;
			// 
			// Title_label
			// 
			this.Title_label.AutoSize = true;
			this.Title_label.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Title_label.Location = new System.Drawing.Point(12, 168);
			this.Title_label.Name = "Title_label";
			this.Title_label.Size = new System.Drawing.Size(49, 26);
			this.Title_label.TabIndex = 2;
			this.Title_label.Text = "Title";
			// 
			// Price_label
			// 
			this.Price_label.AutoSize = true;
			this.Price_label.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Price_label.Location = new System.Drawing.Point(12, 194);
			this.Price_label.Name = "Price_label";
			this.Price_label.Size = new System.Drawing.Size(54, 26);
			this.Price_label.TabIndex = 3;
			this.Price_label.Text = "Price";
			// 
			// Description_richTextBox
			// 
			this.Description_richTextBox.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Description_richTextBox.Location = new System.Drawing.Point(171, 12);
			this.Description_richTextBox.Name = "Description_richTextBox";
			this.Description_richTextBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.Description_richTextBox.Size = new System.Drawing.Size(351, 234);
			this.Description_richTextBox.TabIndex = 4;
			this.Description_richTextBox.Text = "";
			// 
			// DiscountedPrice_label
			// 
			this.DiscountedPrice_label.AutoSize = true;
			this.DiscountedPrice_label.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.DiscountedPrice_label.Location = new System.Drawing.Point(12, 220);
			this.DiscountedPrice_label.Name = "DiscountedPrice_label";
			this.DiscountedPrice_label.Size = new System.Drawing.Size(0, 26);
			this.DiscountedPrice_label.TabIndex = 5;
			// 
			// Buy_button
			// 
			this.Buy_button.Location = new System.Drawing.Point(17, 275);
			this.Buy_button.Name = "Buy_button";
			this.Buy_button.Size = new System.Drawing.Size(149, 23);
			this.Buy_button.TabIndex = 7;
			this.Buy_button.Text = "Add to Cart";
			this.Buy_button.UseVisualStyleBackColor = true;
			this.Buy_button.Click += new System.EventHandler(this.Buy_button_Click);
			// 
			// Discounted_pictureBox
			// 
			this.Discounted_pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("Discounted_pictureBox.Image")));
			this.Discounted_pictureBox.Location = new System.Drawing.Point(116, 170);
			this.Discounted_pictureBox.Margin = new System.Windows.Forms.Padding(2);
			this.Discounted_pictureBox.Name = "Discounted_pictureBox";
			this.Discounted_pictureBox.Size = new System.Drawing.Size(50, 50);
			this.Discounted_pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.Discounted_pictureBox.TabIndex = 8;
			this.Discounted_pictureBox.TabStop = false;
			this.Discounted_pictureBox.Visible = false;
			// 
			// Category_label
			// 
			this.Category_label.AutoSize = true;
			this.Category_label.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Category_label.Location = new System.Drawing.Point(12, 246);
			this.Category_label.Name = "Category_label";
			this.Category_label.Size = new System.Drawing.Size(88, 26);
			this.Category_label.TabIndex = 9;
			this.Category_label.Text = "Category";
			// 
			// SingleProduct
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.MistyRose;
			this.ClientSize = new System.Drawing.Size(534, 310);
			this.Controls.Add(this.Category_label);
			this.Controls.Add(this.Discounted_pictureBox);
			this.Controls.Add(this.Buy_button);
			this.Controls.Add(this.DiscountedPrice_label);
			this.Controls.Add(this.Description_richTextBox);
			this.Controls.Add(this.Price_label);
			this.Controls.Add(this.Title_label);
			this.Controls.Add(this.ProductPic_pictureBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Name = "SingleProduct";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Single Product";
			this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.Load += new System.EventHandler(this.SingleProduct_Load);
			((System.ComponentModel.ISupportInitialize)(this.ProductPic_pictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Discounted_pictureBox)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

		#endregion

		private System.Windows.Forms.PictureBox ProductPic_pictureBox;
		private System.Windows.Forms.Label Title_label;
		private System.Windows.Forms.Label Price_label;
		private System.Windows.Forms.RichTextBox Description_richTextBox;
		private System.Windows.Forms.Label DiscountedPrice_label;
		private System.Windows.Forms.Button Buy_button;
		private System.Windows.Forms.PictureBox Discounted_pictureBox;
		private System.Windows.Forms.Label Category_label;
	}
}