﻿using System.Collections.Generic;
using System.Windows.Forms;
using DigiTablo.Models;
using DigiTablo.Utils;
using DigiTablo.Views;

namespace DigiTablo
{
    static class Program
    {
        public static readonly Database Database = new Database();
        public static readonly List<Product> Cart = new List<Product>();

        static void Main()
        {
            Database.Open();
            Application.Run(new MainForm());
        }

        public static void OpenProductAdding()
		{
            (new ProductAdding()).ShowDialog();
		}

        public static void Close()
        {
			Database.Close();
			Application.Exit();
        }
    }
}