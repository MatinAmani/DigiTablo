﻿using System.Collections.Generic;
using System.Linq;
using DigiTablo.Models;

namespace DigiTablo.Utils
{
    public class Authentication
    {
        public bool Login(string username, string password)
        {
            List<User> users = User.Where("username", username);

            if (users.Count == 0)
            {
                return false;
            }

            return users.First().password == password;
        }
    }
}