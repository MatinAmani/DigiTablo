﻿using MySqlConnector;
using System;
using System.Configuration;

namespace DigiTablo.Utils
{
    public class Database
    {
        private readonly MySqlConnection _connection;

        public Database()
        {
            this._connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["MySql"].ConnectionString);
        }

        public void Open()
        {
            this._connection.Open();
        }

        public void Close()
        {
            this._connection.Close();
        }

        public bool TestConnection()
        {
            MySqlCommand command = new MySqlCommand("show tables;", this._connection);
            try
            {
                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception e)
            {
                // ignored
            }

            return false;
        }

        public bool Insert(string table, string[] columns, string values)
        {
            string parsedColumns = this.concat(columns);
            string query = "INSERT INTO " + table + " " + parsedColumns + " VALUES " + values + ";";

            MySqlCommand command = new MySqlCommand(query, this._connection);
            return command.ExecuteNonQuery() == 1;
        }

        public bool Delete(string table, int id)
        {
            string query = "DELETE FROM " + table + " WHERE id=" + id + ";";
            MySqlCommand command = new MySqlCommand(query, this._connection);
            return command.ExecuteNonQuery() == 1;
        }

        public MySqlDataReader Get(string table, int id)
        {
            string query = "SELECT * FROM " + table + " WHERE id=" + id + " LIMIT 1;";
            MySqlCommand command = new MySqlCommand(query, this._connection);
            return command.ExecuteReader();
        }

        public MySqlDataReader Get(string table, string column, string value, bool contains = false)
        {
            string where = " WHERE " + column + "='" + value + "'";
            if (contains)
            {
                where = " WHERE " + column + " like '%" + value + "%'";
            }

            string query = "SELECT * FROM " + table + where + ";";
            MySqlCommand command = new MySqlCommand(query, this._connection);
            return command.ExecuteReader();
        }

        public MySqlDataReader Get(string table)
        {
            string query = "SELECT * FROM " + table + ";";
            MySqlCommand command = new MySqlCommand(query, this._connection);
            return command.ExecuteReader();
        }

        private string concat(string[] parts)
        {
            string result = "(";

            for (int i = 0; i < parts.Length; i++)
            {
                result = result + parts[i];
                if (i != parts.Length - 1)
                {
                    result = result + ", ";
                }
            }
            
            result = result + ")";

            return result;
        }
    }
}